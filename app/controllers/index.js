function doClick(e) {
    alert($.label.text);
}

/*
 * Runs on window open
 */
function windowOpen() {

	Alloy.Collections.api.fetch({
		url: "http://jsonplaceholder.typicode.com/posts",
		// uncomment and add data to post
		//data : {},
		add: true,
		silent: true,
		processData : false,
		success : function(_model, _response) {
			Ti.API.info(_response);
		},
		error : function(_model, _response) {
			Ti.API.info('Events Fetch Error');
			Ti.API.info(_response);
		}
	});
}

$.index.open();