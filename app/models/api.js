exports.definition = {
    config: {
        debug: 1,
        URL: 'http://jsonplaceholder.typicode.com/posts',
        adapter: {
            type: "restapi",
            collection_name: "api"
        }
    },      
    extendModel: function(Model) {      
        _.extend(Model.prototype, {
            // extended functions and properties go here
        });
 
        return Model;
    },
    extendCollection: function(Collection) {        
        _.extend(Collection.prototype, {
            // extended functions and properties go here
        });
 
        return Collection;
    }
};